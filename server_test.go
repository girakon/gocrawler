package main

import (
	"testing"
)

func TestResponseJson(t *testing.T) {
	resp := new(Response)
	resp.Ok("")
	if result := resp.Json(); result != "{\"result\":\"ok\"}" {
		t.Errorf("Json breaked! %s", result)
	}
}
