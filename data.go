package main

type Data struct {
	Url   string               `json:"url"`
	Time  string               `json:"time"`
	Items map[string]*DataItem `json:"items,omniempty"`
}

type DataItem struct {
	Selector string               `json:"selector"`
	Data     string               `json:"data"`
	Items    map[string]*DataItem `json:"items,omniempty"`
}

type DataItems map[string]*DataItem
