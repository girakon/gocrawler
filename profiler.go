package main

import (
	"fmt"
	"time"
)

type Profiler struct {
	Data  ProfileResults
	Stack ProfileResults
}

type ProfilerResult struct {
	From     time.Time
	To       time.Time
	Duration time.Duration
}

type ProfileResults map[string]*ProfilerResult

func (p *Profiler) Start(name string) {
	result := new(ProfilerResult)
	result.From = time.Now()
	p.Stack[name] = result
}

func (p *Profiler) Stop(name string) {
	result, exist := p.Stack[name]
	if !exist {
		return
	}
	result.To = time.Now()
	result.Duration = result.To.Sub(result.From)
	p.Data[name] = result
	delete(p.Stack, name)
}

func (p *Profiler) Stat() {
	for name, result := range p.Data {
		fmt.Printf("%s : %f ms\n", name, float64(result.Duration/time.Millisecond))
	}
}

func (p *Profiler) Get(name string) string {
	result, exist := p.Data[name]
	if !exist {
		return "false"
	}
	return fmt.Sprintf("%f", float64(result.Duration/time.Millisecond))
}

func NewProfiler() *Profiler {
	return &Profiler{make(ProfileResults), make(ProfileResults)}
}
