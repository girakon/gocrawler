// server
package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type Response struct {
	Result  string `json:"result"`
	Message string `json:"message,omitempty"`
	Data    *Data  `json:"data,omitempty"`
	Time    string `json:"time,omniempty"`
}

type ResponseData struct {
	Result string `json:"result"`
}

func (r *Response) Error(message string) string {
	r.Result = "error"
	r.Message = message
	return r.Json()
}

func (r *Response) Json() string {
	j, _ := json.Marshal(r)
	return string(j)
}

func (r *Response) Ok(message string) string {
	r.Result = "ok"
	r.Message = message
	return r.Json()
}

func handleParse(w http.ResponseWriter, r *http.Request) {
	resp := new(Response)
	profiler := NewProfiler()
	body, err := ioutil.ReadAll(r.Body)
	r.Body.Close()
	if err != nil {
		fmt.Fprintf(w, resp.Error(err.Error()))
		return
	}
	options := new(Options)
	err2 := json.Unmarshal(body, options)
	if err2 != nil {
		fmt.Fprintf(w, resp.Error(err2.Error()+string(body)))
		return
	}
	fetcher := NewFetcher(options)
	profiler.Start("fetch")
	fetcher.Fetch()
	profiler.Stop("fetch")
	resp.Data = fetcher.Data
	resp.Time = profiler.Get("fetch")
	//fmt.Println(fetcher.Json())

	fmt.Fprintf(w, resp.Ok("fetched"))
	//url := r.FormValue("url")
	//if url == "" {
	//	fmt.Fprintf(w, resp.Error("Provide url!"))
	//	return
	//}
	//profiler.Start("get")
	//response, err := http.Get(url)
	//profiler.Stop("get")
	//profiler.Stat()
	//resp.Time = profiler.Get("get")
	//if err != nil {
	//	resp.Error(err.Error())
	//	return
	//}
	//body, err2 := ioutil.ReadAll(response.Body)
	//response.Body.Close()
	//if err2 != nil {
	//	resp.Error(err2.Error())
	//	return
	//}
	//resp.SetData(string(body))
	//fmt.Fprintf(w, resp.Ok(url))

}

func serve(port string) {
	fmt.Printf("Server started: %s \n", port)
	http.HandleFunc("/api/parse", handleParse)
	e := http.ListenAndServe(port, nil)
	if e != nil {
		fmt.Println(e.Error())
	}
	fmt.Println("Server shutdown \n")

}
