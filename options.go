package main

import ()

type Options struct {
	Url   string                  `json:"url"`
	Fetch map[string]*FetchOption `json:"fetch"`
}

type FetchOption struct {
	Selector string                  `json:"selector"`
	Fetch    map[string]*FetchOption `json:"fetch"`
}

type FetchOptions map[string]*FetchOption
