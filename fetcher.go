package main

import (
	"encoding/json"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	//"html"
	"io"
	//"io/ioutil"
	"net/http"
)

type Fetcher struct {
	Options *Options
	Data    *Data
}

func NewFetcher(optios *Options) *Fetcher {
	return &Fetcher{optios, new(Data)}
}

func (f *Fetcher) Fetch() {
	response, err := http.Get(f.Options.Url)
	if err != nil {
		fmt.Printf("Error: %s \n", err.Error())
		return
	}
	f.FetchNode(response.Body)

}

func (f *Fetcher) FetchNode(r io.Reader) {
	document, err := goquery.NewDocumentFromReader(r)
	if err != nil {
		fmt.Printf("Error: %s \n", err.Error())
		return
	}
	data := new(Data)
	data.Url = f.Options.Url
	data.Items = make(map[string]*DataItem)
	profiler := NewProfiler()
	profiler.Start("fetch all")
	for name, fetch := range f.Options.Fetch {
		item := new(DataItem)
		item.Selector = fetch.Selector
		selection := document.Find(fetch.Selector)
		if selection.Length() > 1 {
			selection.Each(func(index int, sel *goquery.Selection) {
				s, _ := sel.Html()

				item.Data += s
			})
		} else {
			item.Data, _ = selection.Html()
		}

		data.Items[name] = item
	}
	profiler.Stop("fetch all")
	data.Time = profiler.Get("fetch all")
	f.Data = data
}

func (f *Fetcher) Json() string {
	str, err := json.Marshal(f.Data)
	if err != nil {
		return err.Error()
	}
	return string(str)
}
